import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import App from './Components/App';

import store from './Redux/store.js';

import './style/fonts/font-tui.css';
import './style/all.css'

ReactDOM.render(
    <Provider store = { store } >
        <App />
    </Provider>, 
    document.getElementById('root')
);
