import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import {LogIn, banners} from '../Reducers';

const middleware = applyMiddleware(
    thunk,
);

const reducers = combineReducers({
    auth: LogIn,
    banners,
  });


const store = createStore( reducers, composeWithDevTools( middleware ) );

export default store;