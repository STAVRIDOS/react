-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Окт 07 2019 г., 07:15
-- Версия сервера: 5.7.27-0ubuntu0.16.04.1
-- Версия PHP: 7.0.33-0ubuntu0.16.04.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `tui-admin`
--

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `user_login` varchar(30) NOT NULL,
  `user_password` mediumtext NOT NULL,
  `user_hash` varchar(32) NOT NULL DEFAULT '',
  `user_ip` varchar(10) NOT NULL DEFAULT '0',
  `user_surname` mediumtext,
  `user_photo` mediumtext NOT NULL,
  `user_department` varchar(50) NOT NULL,
  `user_role` varchar(50) NOT NULL,
  `user_name` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`user_id`, `user_login`, `user_password`, `user_hash`, `user_ip`, `user_surname`, `user_photo`, `user_department`, `user_role`, `user_name`) VALUES
(1, 'a.umanets@tui.ua', '$2y$10$xmpTHEqxLftepLhiBYTvaOtwvwl3ex57V6kbGNyd.gyGac1kx16Ii', 'e4e2c55c86caba62eace747b804f0e70', '127.0.0.1', 'Уманець', 'https://lh3.googleusercontent.com/-Rg_01Vmfq9E/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rfOa8j9S5HT5fWbo9z9GbT63AVf5Q.CMID/s32-c/photo.jpg', 'web', 'Администратор', 'Олександр'),
(2, 'v.cholkin@tui.ua', '$2y$10$vCh6O0vzUtdMcDz/cVmAZ.PkQsJO1jSigWGUchkfKXUNsPIJlKrre', '', '127.0.0.1', 'Чолкин', 'https://timesofindia.indiatimes.com/thumb/msid-67586673,width-800,height-600,resizemode-4/67586673.jpg', 'product', 'Редактор', 'Вася');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `user_department` (`user_department`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
