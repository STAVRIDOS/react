<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 0.2b
 */

//
// Database `adminos`
//

// `adminos`.`users`
$users = array(
  array('user_id' => '1','user_login' => 'a.umanets@tui.ua','user_password' => '$2y$10$xmpTHEqxLftepLhiBYTvaOtwvwl3ex57V6kbGNyd.gyGac1kx16Ii','user_hash' => '13d75de742105537fa93b372513ae51d','user_ip' => '127.0.0.1','user_surname' => 'Уманець','user_photo' => 'https://lh3.googleusercontent.com/-Rg_01Vmfq9E/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rfOa8j9S5HT5fWbo9z9GbT63AVf5Q.CMID/s32-c/photo.jpg','user_department' => 'web','user_role' => 'administrator','user_name' => 'Олександр','user_test' => '1')
);
