-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Окт 03 2019 г., 01:12
-- Версия сервера: 5.7.27-0ubuntu0.16.04.1
-- Версия PHP: 7.0.33-0ubuntu0.16.04.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `adminos`
--

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `user_id` int(40) NOT NULL,
  `user_login` varchar(100) NOT NULL,
  `user_password` varchar(500) NOT NULL,
  `user_hash` varchar(500) NOT NULL,
  `user_ip` varchar(100) NOT NULL,
  `user_surname` varchar(100) NOT NULL,
  `user_photo` text NOT NULL,
  `user_department` varchar(200) NOT NULL,
  `user_role` varchar(100) NOT NULL,
  `user_name` text NOT NULL,
  `user_test` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`user_id`, `user_login`, `user_password`, `user_hash`, `user_ip`, `user_surname`, `user_photo`, `user_department`, `user_role`, `user_name`, `user_test`) VALUES
(1, 'a.umanets@tui.ua', '$2y$10$xmpTHEqxLftepLhiBYTvaOtwvwl3ex57V6kbGNyd.gyGac1kx16Ii', '13d75de742105537fa93b372513ae51d', '127.0.0.1', 'Уманець', 'https://lh3.googleusercontent.com/-Rg_01Vmfq9E/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rfOa8j9S5HT5fWbo9z9GbT63AVf5Q.CMID/s32-c/photo.jpg', 'web', 'administrator', 'Олександр', 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(40) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
