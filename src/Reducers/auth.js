import { LOGIN, LOGOUT, AUTH_IT, GET_DATA, TEMPORARY_ACCESS} from '../Actions';

export const LogIn = (state = {}, action) => {
    switch (action.type){
        case LOGIN:
            return {
                ...state,
                user: action.payload.user
            }
        case LOGOUT:
            return {
                user: {
                    auth: false,
                    login: true,
                    password: true,
                    ip: true
                }
            }
        case AUTH_IT:
            
            return {
                ...state,
                ...action.payload
            }
        case GET_DATA:
            return {
                ...state,
                pages: {
                    ...state.pages,
                    ...action.payload.pages,
                    activePage: 0
                }
            }
        case 'USERS_ONLINE':
            return {
                ...state,
                usersOnline: action.payload
            }
        
        case 'UPDATE_PASSWORD':
            return {
                ...state,
                passwordStatus: action.payload
            }

        default: 
            return state;
    }
}

export default LogIn;