import { 
    SORT_BANNERS, 
    GET_BANNERS, 
    ACTIVE_PAGE, 
    LOADING, 
    DELITE_BANNERS, 
    UPDATE_BANNERS, 
    ADD_BANNERS, 
    ALL_DELITE, 
    ALL_VISIBILITY,
    SORT_TYPE_BANNERS,
    SEARCH_BANNERS
} from '../Actions';

const init ={
    activePage: 0,
    loaded: true,
    allBanners: [],
    activeSite: false,
    aciveTypeBanner: false,
    resultBanners: []
}

export const banners = (state = init, action) => {
    switch (action.type){
        case LOADING :
            return {
                ...state,
                loaded: action.loading
            }
        case SORT_BANNERS:
            let sortSiteBanners = (()=>{
                let count = 17;
                let result = [];
                
                if(state.allBanners !== undefined){
                    if(action.payload === 'Все'){
                        if(state.allBanners.length > count){
             
                            for(let i = 0; i < Math.ceil(state.allBanners.length / count); i++){
                                result.push(state.allBanners.slice(i*count, (i*count)+count))
                            }
                        }else{
                            result = [
                                [
                                    ...state.allBanners
                                ]
                            ]
                        }
                    }else{
                        let sortArr = state.allBanners.filter( item =>{ 
                            return item.site === action.payload 
                        })

                        let arrLeng = (sortArr.length < count) ? 1 : Math.ceil(sortArr.length / count);
        
                        for(let i = 0; i < arrLeng; i++){
                            result.push(sortArr.slice(i*count, (i*count)+count))
                        }

                    }
                }

                return result;
            })()

            return {
                ...state,
                activeSite: action.payload,
                resultSortSites: sortSiteBanners,
                resultBanners: sortSiteBanners,
                activePage: state.activePage,
          
            }

        case GET_BANNERS:  
            let sortBanners = (()=>{
                let count = 17;
                let result = [];

                for(let i = 0; i < action.payload.length / count; i++){
                    result.push(action.payload.slice(i*count, (i*count)+count))
                }                 
                return result;
            })();
            return {
                ...state,
                allBanners: action.payload,
                resultBanners:  sortBanners,
                activePage: state.activePage,
                loaded: true,
                activeSite: action.site,
                
            }
        case ALL_DELITE :  
            return {
                ...state,
                allBanners: [...action.payload],
                resultBanners:  (()=>{
                    let count = 17;
                    let result = [];

                    for(let i = 0; i < action.payload.length / count; i++){
                        result.push(action.payload.slice(i*count, (i*count)+count))
                    }                 
                    return result;
                })(),
                loaded: true
            }
        case ALL_VISIBILITY :  
            return {
                ...state,
                allBanners: [...action.payload],
                resultBanners:  (()=>{
                    let count = 17;
                    let result = [];

                    for(let i = 0; i < action.payload.length / count; i++){
                        result.push(action.payload.slice(i*count, (i*count)+count))
                    }                 
                    return result;
                })(),
                loaded: true,
            }
        case DELITE_BANNERS :  
            return {
                ...state,
                allBanners: [...action.payload],
                resultBanners:  (()=>{
                    let count = 17;
                    let result = [];

                    for(let i = 0; i < action.payload.length / count; i++){
                        result.push(action.payload.slice(i*count, (i*count)+count))
                    }                 
                    return result;
                })(),
                loaded: true
            }
        case UPDATE_BANNERS :  
            return {
                ...state,
                allBanners: [...action.payload],
                resultBanners:  (()=>{
                    let count = 17;
                    let result = [];

                    for(let i = 0; i < action.payload.length / count; i++){
                        result.push(action.payload.slice(i*count, (i*count)+count))
                    }                 
                    return result;
                })(),
                loaded: true
            }
        case ADD_BANNERS :  
            return {
                ...state,
                allBanners: [...action.payload],
                resultBanners:  (()=>{
                    let count = 17, result = [];

                    for(let i = 0; i < action.payload.length / count; i++){
                        result.push(action.payload.slice(i*count, (i*count)+count))
                    }                 
                    return result;
                })(),
                loaded: true
            }
            
        case ACTIVE_PAGE :
            
            return {
                ...state,
                activePage: (action.payload > state.resultBanners.length-1 || action.payload < 0) ? 0 : action.payload
            }

        case SEARCH_BANNERS:
        
                return {
                    ...state
                }

        case SORT_TYPE_BANNERS:

                return {
                    ...state,
                    aciveTypeBanner: action.payload,
                    resultBanners: (()=>{
                        let count = 17, result = [];
                        
                        let sortArr = state.allBanners.filter( item =>{ 
                            return item.type === action.payload 
                        })

                        for(let i = 0; i < sortArr.length / count; i++){
                            result.push(sortArr.slice(i*count, (i*count)+count))
                        }

                        return result;
                    })(),
                    activePage: 0
                }

        default: 
            return state;
    }
}

export default banners;