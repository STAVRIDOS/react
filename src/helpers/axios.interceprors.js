import axios from 'axios';

import store from '../Redux/store';

const queryAdmin = axios.create({
    baseURL: 'http://localhost/index.php'
    // baseURL: '/admin.php'
});

queryAdmin.interceptors.response.use(res => {
    if(res.data.user){
        if(res.data.user.auth){
          store.dispatch({type: 'USERS_ONLINE', payload: res.data.user.users_online});
      }else{
        store.dispatch({type: 'LOGOUT'});
      }
      return res; 
    }
  }, error => {
    store.dispatch({type: 'LOGOUT'})
  });

export default queryAdmin;