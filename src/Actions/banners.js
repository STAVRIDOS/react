
import queryAdmin from '../helpers/axios.interceprors';

export const LOADING = 'LOADING';
export const SORT_BANNERS = 'SORT_BANNERS';
export const GET_BANNERS = 'GET_BANNERS';
export const ADD_BANNERS = 'ADD_BANNERS';
export const UPDATE_BANNERS = 'UPDATE_BANNERS';
export const DELITE_BANNERS = 'DELITE_BANNERS';
export const ACTIVE_PAGE = 'ACTIVE_PAGE';
export const ERROR_REQUES = 'ERROR_REQUES';
export const ALL_VISUALE = 'ALL_VISUALE';
export const ALL_DELITE = 'ALL_DELITE';
export const ALL_VISIBILITY = 'ALL_VISIBILITY';
export const SORT_TYPE_BANNERS = 'SORT_TYPE_BANNERS';
export const SEARCH_BANNERS = 'SEARCH_BANNERS';



export const getBanners = site => (dispatch, getState)=>{  
    dispatch({
        type: LOADING,
        loading: false
    })
    queryAdmin({
        method: 'post',
        withCredentials: true,
        data: JSON.stringify({
            page: 'banners',
            site: site
        })
    }).then(res=>{
            dispatch({
                type: GET_BANNERS,
                payload: res.data.banners,
                site: site
            })
    }).catch(error =>{
        dispatch({
            type: ERROR_REQUES
        })
    });
}

export const sortBanners = value => (dispatch, getState)=>{  
    dispatch({
        type: SORT_BANNERS,
        payload: value
    })
}

export const addBanners = value => (dispatch, getState)=>{  
    dispatch({
        type: LOADING,
        loading: false
    })
    
    queryAdmin({
        method: 'post',
        data: JSON.stringify({
            page: 'banners', 
            type: ADD_BANNERS,
            banner: {
                ...value,
                start: new Date(value.start).getTime(),
                finish: new Date(value.finish).getTime()
            }
        }),
        withCredentials: true
    }).then(res=>{
        dispatch({
            type: ADD_BANNERS,
            payload: res.data.banners
        })
        
    })
}

export const updateBanners = value => (dispatch, getState)=>{  
    dispatch({
        type: LOADING,
        loading: false
    })
    queryAdmin({
        method: 'post',
        data: JSON.stringify({
            page: 'banners', 
            type: UPDATE_BANNERS,
            banner: {
                ...value,
                start: new Date(value.start).getTime(),
                finish: new Date(value.finish).getTime()
            }
        }),
        withCredentials: true
    }).then(res =>{
        console.log(res.data.banners)
        dispatch({
            type: UPDATE_BANNERS,
            payload: res.data.banners
        })

    }).catch(error =>{
        dispatch({
            type: ERROR_REQUES
        })
    });
}

export const deleteBanners = value => (dispatch, getState)=>{ 
    console.log(value.name)
    let status = window.confirm('Вы дейтвительно хотите удалить баннер: '+value.name);
    if(status){
        dispatch({
            type: LOADING,
            loading: false
        })
        queryAdmin({
            method: 'post',
            data: JSON.stringify({
                page: 'banners',
                type: DELITE_BANNERS,
                banner: {
                    ...value
                }
            }),
            withCredentials: true
        }).then(res => {
            dispatch({
                type: DELITE_BANNERS,
                payload: res.data.banners
            })
    
        }).catch(error =>{
            dispatch({
                type: ERROR_REQUES
            })
        });
    }else{
        return false
    }
    
}

export const allDelite = value => (dispatch, getState)=>{  
    let status = window.confirm('Подтвердить удаление?');
    if(status){
        dispatch({
            type: LOADING,
            loading: false
        })
        queryAdmin({
            method: 'post',
            data: JSON.stringify({
                page: 'banners',
                type: ALL_DELITE,
                banner: [
                    ...value
                ]
            }),
            withCredentials: true
        }).then(res => {
            dispatch({
                type: ALL_DELITE,
                payload: res.data.banners
            })
    
        })
    }
}

export const allVisibility = value => (dispatch, getState)=>{  
    dispatch({
        type: LOADING,
        loading: false
    })
    queryAdmin({
        method: 'post',
        data: JSON.stringify({
            page: 'banners',
            type: ALL_VISIBILITY,
            banner: [
                ...value
            ]
        }),
        withCredentials: true
    }).then(res => {
        dispatch({
            type: ALL_VISIBILITY,
            payload: res.data.banners
        })

    })
}

export const sortTypeBanners = type => (dispatch, getState)=>{ 
    dispatch({
        type: SORT_TYPE_BANNERS,
        payload: type
    })
}

export const searchBanners = name => (dispatch, getState)=>{ 
    dispatch({
        type: SEARCH_BANNERS,
        payload: name
    })
}

export const activePage = number => (dispatch, getState)=>{  
    console.log(number)
    dispatch({
        type: ACTIVE_PAGE,
        payload: number
    })
}



