
export const SORT_TYPE_BANNERS = 'SORT_TYPE_BANNERS';

export const sortTypeBanners = type => (dispatch, getState)=>{ 
    dispatch({
        type: SORT_TYPE_BANNERS,
        payload: type
    })
}
