import queryAdmin from '../helpers/axios.interceprors';

export const QUERY = 'QUERY';
export const RES = 'RES';
export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const AUTH_IT = 'AUTHIT';
export const GET_DATA = 'GET_DATA';
export const SORT_BANNERS = 'SORT_BANNERS';
export const GET_BANNERS = 'GET_BANNERS';
export const USERS_ONLINE = 'USERS_ONLINE';
export const TEMPORARY_ACCESS = 'TEMPORARY_ACCESS';
export const UPDATE_PASSWORD = 'UPDATE_PASSWORD';



export const LogIn = query => (dispatch, getState)=>{  
    queryAdmin(query).then(res=>{
            dispatch({
                type: LOGIN,
                payload: res.data
            })
    })
}

export const LogOut = () => (dispatch, getState)=>{
    queryAdmin({
        method: 'put',
        withCredentials: true
    }).then(res=>{
        
        dispatch({
            type: LOGOUT,
            payload: []
        })
    })
}


export const authIt = () => (dispatch, getState)=>{
    queryAdmin({
        method: 'post',
        withCredentials: true
    }).then(res=>{
            dispatch({
                type: AUTH_IT,
                payload: res.data
            })
    })
}

export const getData = () => (dispatch, getState)=>{
    queryAdmin({
        method: 'post',
        withCredentials: true
    }).then(res=>{
            dispatch({
                type: GET_DATA,
                payload: res.data
            })
    })
}

export const temporaryIP = code => (dispatch, getState)=>{
    queryAdmin({
        method: 'post',
        data: JSON.stringify({
            code: code,
            page: 'ta'
        }),
        withCredentials: true
    })
}

export const updatePassword = (password, id) => (dispatch, getState) =>{
    queryAdmin({
        method: 'post',
        auth: {
            password: password
        },
        data: JSON.stringify({
            newPass: 'updatePassword',
            id: id
        }),
        withCredentials: true
    }).then(res =>{
        dispatch({
            type: UPDATE_PASSWORD,
            payload: res.data.newPass
        })
    }).catch(error =>{
        console.log('aaaaaaaaaaaaaaaaaaaaa')
    })
}
