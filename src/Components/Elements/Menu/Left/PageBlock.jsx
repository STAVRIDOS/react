import React from 'react';
import { NavLink } from 'react-router-dom';

export const PageBlock = ({ blocks, role, getData, site, activeSite}) => {
    return (
        <div className="navigations__row">
            {
                blocks.map(block=>{
                    
                    return block.role.map((user_role, index)=>{
                        if(role === user_role){
                            return (
                            <NavLink 
                                key={index} 
                                to={block.link} 
                                activeClassName="activeMenuBlock" 
                                className="navigation__item"
                                onClick={()=>{block.query(site)}}
                                
                            >
                                <div  className="item__info">
                                    <div className="item__icon">
                                        {block.img}
                                    </div>
                                    <div className="item__name">
                                        {block.name}
                                    </div>
                                </div>
                            </NavLink>
                            ) 
                        }else{
                            return ''
                        }
                    })
                })
            }            
        </div>
    )
}

export default PageBlock;
