import React from 'react'

export const UsersOnline = ({users}) => {
    return (
        <>
            <div className="titile__users-online">пользователи <span style={{color: 'green'}}>online</span></div>
            <div className="usersOnline">
                {
                    users.map((user,index)=>(
                        <div key={index} className="user_online">
                            <div className="user__photo">
                                <img src={user.photo} alt=""/>
                            </div>
                            <div className="user__name">
                                {user.name} {user.surname}
                            </div>
                        </div>
                    ))
                }
            </div>
        </>
    )
}

// import React, { Component } from 'react';

// // import '../../../style/users.css';

// export class UsersOnline extends Component {

//     render() {
//         let { users } = this.props;
//         console.log(this.props)
//         return (
//             <>
//                 <div className="titile__users-online">пользователи <span style={{color: 'green'}}>online</span></div>
//                 <div className="usersOnline">
//                     {
//                         users.map((user,index)=>(
//                             <div key={index} className="user_online">
//                                 <div className="user__photo">
//                                     <img src={user.photo} alt=""/>
//                                 </div>
//                                 <div className="user__name">
//                                     {user.name} {user.surname}
//                                 </div>
//                             </div>
//                         ))
//                     }
//                 </div>
//             </>
//         )
//     }
// }

// UsersOnline.defaultProps = {
//     users_online: []
// }

export default UsersOnline;