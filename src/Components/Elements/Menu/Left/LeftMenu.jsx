import React, { Component } from 'react';
import { NavLink } from 'react-router-dom'
import PageBlock from './PageBlock';
import UsersOline from './UsersOnline';

import { FaGroup, FaCogs, FaServer, FaFileO, FaRetweet, FaImage, FaAlignLeft } from 'react-icons/lib/fa';

import './left-menu.css';

export class LeftMenu extends Component {
    state = {
        blocks: [
            {
                name: 'Страницы',
                img: <FaFileO />,
                link: '/pages',
                role: ['administrator','editor'],
                query: ()=>{}
            },
            {
                name: 'Баннеры', 
                img: <FaImage />,
                link: '/banners',
                role: ['administrator','editor'],
                query: this.props.getBanners
            },
            {
                name: 'Формы',
                img: <FaAlignLeft />,
                link: '/forms',
                role: ['administrator','editor'],
                query: ()=>{}
            },
            {
                name: 'Парсер',
                img: <FaRetweet />,
                link: '/parser',
                role: ['administrator','editor'],
                query: ()=>{}
            },
            {
                name: 'Дополнительные сервисы',
                img: <FaServer />,
                link: '/service',
                role: ['administrator','editor'],
                query: ()=>{}
            },
            {
                name: 'Пользователи',
                img: < FaGroup />,
                link: '/users',
                role: ['administrator','editor'],
                query: ()=>{}
            },
            {
                name: 'Настройки',
                img: < FaCogs />,
                link: '/settings',
                role: ['administrator','editor'],
                query: ()=>{}
                
            }
        ]
    }
    render() {
        let { users, site } = this.props;

        return (
            <>
                <NavLink  className="button__home" to="/">Главная страница</NavLink>
                <PageBlock  
                    blocks={this.state.blocks} 
                    role={this.props.user.role}
                    site={site}
                />
                <UsersOline users={users}  />
            </>
        )
    }
}

export default LeftMenu;
