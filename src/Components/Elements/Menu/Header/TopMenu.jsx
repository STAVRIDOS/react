import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';

import { LogOut } from '../../../../Actions';

import {FaSignOut, FaCommentsO, FaEnvelopeO, FaSkype} from 'react-icons/lib/fa';

import './header-menu.css';

export class TopMenu extends Component {

    render() {

        let {name, surname, photo, department } = this.props.user;
        
        return (
            <div className="header__menu">
                <div className="header__left">
                    <div className="chat icons__top">
                        <FaCommentsO />
                        <div className='icon__text'>chat</div>
                        <span className="count__icons">5</span>
                    </div>
                    <div className="mail icons__top">
                        <FaEnvelopeO />
                        <div className='icon__text'>mail</div>
                        <span className="count__icons">3</span>
                    </div>
                    <div className="skype icons__top">
                        <FaSkype />
                        <div className='icon__text'>chat</div>
                        <span className="count__icons">0</span>
                    </div>
                </div>
                <div className="row-info__user">
                    <NavLink  to='/profile'  activeClassName="activeMenuBlocksksdjklfsdfjk"  className="header__right" >
                        <div>
                            <span className="name">{ name }</span>
                            <span className="surname"> { surname }</span>
                            <div>Отдел: <span className="user__department">{department}</span></div>
                        </div>
                        <span className="photo">
                            <img src={photo} alt=""/>
                        </span>
                    </NavLink>
                    <span className='exit' onClick={this.props.LogOut}><FaSignOut /></span>
                </div>
            </div>
        )
    }
}


const mapDispatchToProps = (dispatch, getState) =>{
    return ({
        LogOut: () => {
            dispatch(LogOut())
        }
    })
}

export default connect(null, mapDispatchToProps)(TopMenu);
