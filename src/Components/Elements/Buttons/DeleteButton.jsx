import React from 'react';
import { NavLink } from 'react-router-dom';
import './buttons.css';

import { FaTimesCircle, FaCheck } from 'react-icons/lib/fa';

export const DeleteButton = ({handler, type, banner}) => {
    return (
        <>
        {
            type === 'del' ? (
                <NavLink to='/banners'  className='delete_button' onClick={handler(banner)}> 
                    <FaTimesCircle />
                 
                    <span className="button__text">Удалить</span>
                </NavLink>
            ) : 
            type === 'save' ? (
                <NavLink to='/banners'  className='save_button' onClick={handler(banner)}> 
                    <FaCheck />
                    <span className="button__text">Сохранить</span>
                </NavLink>
            ) : ''
        }
        </>
    )
}

export default DeleteButton;
