import React, { Component } from 'react';
import { connect } from 'react-redux';
import './buttons.css';

export class CheckButton extends Component {
    render() {
        return (
            <div className="row_buttons">
                {
                    this.props.buttons.map((button, key)=>(
                        <div key={key} className={activeButton === key ? "button__blue active_button" : "button__blue "}  onClick={this.buttonClick(button, key)}>
                            {button}
                        </div>
                    ))
                }
            </div>
        )
    }
}
CheckButton.defaultProps = {
    buttons: ['name'],
    index: 0,
    handler: ()=>{
        console.log('handler')
    }
}

const MapStateToProps = state =>{

    return ({
        banners: [
            ...state.banners.pages.allBanners
        ]
    })
}

const mapDispatchToProps = (dispatch, getState) =>{
    return ({
     
    })
}


export default connect(MapStateToProps, mapDispatchToProps)(CheckButton);

