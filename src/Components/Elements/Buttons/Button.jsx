import React, { Component } from 'react';
import './buttons.css';

export class Button extends Component {
    state ={
        activeIndex: null,
        activeSite: this.props.active,
        reload: true
    }
 

    buttonClick = (button, index) => ()=>{
        let { handler } = this.props
      
        this.setState({
            activeIndex: index,
            aciveSite: button
        })

        handler(button);
    }

    componentDidMount(){
        let { buttons,  active} = this.props;

        buttons.forEach((button, key) => {
            if(button === active){
                this.setState({
                    activeIndex: key
                })
            return false
            }
         
        })
    }
    
    UNSAFE_componentWillReceiveProps( nextProps){
        let { buttons} = this.props;
        let { active } = nextProps;
  
    

        if(this.state.reload){
            this.setState({
                reload: false
            })
        }
     
        if(this.props !== nextProps){
            
            buttons.forEach((button, key) => {
                if(button === active){
                    this.setState({
                        activeIndex: key
                    })
                return false
                }
             
            })
            
        }
    }

    render() {
        let { activeIndex } = this.state
        let { title, buttons } = this.props;

        return (
            <div>
                { title !== undefined && <div className="dop__info">{this.props.title} : </div> }
                <div className="row_buttons" style={{gridTemplateColumns: `repeat(${buttons.length  <= 3 ? 2 : buttons.length  / 2}, 1fr)`}}>
                    {
                        buttons.map((button, key)=>{     
                            
                            return (
                                <div key={key} className={activeIndex === key ? "button__blue active_button" : "button__blue "}  onClick={this.buttonClick(button, key)}>
                                    {button}
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        )
    }
}

export default Button;
