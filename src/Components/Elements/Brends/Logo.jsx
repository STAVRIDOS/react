import React from 'react';

import '../../../style/logo.css';
import logo from '../../../img/logo.png';

export const Logo = () => {
    return (
        <div className="logo">
            <div className="logo__img">
                <img src={logo} alt=""/>
            </div>
            <div className="logo__text">
                <span>web</span>
            </div>
        </div>
    )
}

export default Logo;
