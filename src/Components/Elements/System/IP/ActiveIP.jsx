import React, { Component } from 'react';

import './ip.css';

export class ActiveIP extends Component {
    state={
        status_button: true,
        status_input: false,
        code: 0
    }

    newCode = () =>{
        let {temporaryIP} = this.props;
        temporaryIP(Number(document.getElementById('code').value));
    }

    activeCode = () => {
        let {temporaryIP} = this.props;
        this.setState({
            status_button: false,
            status_input: true
        })
   
        temporaryIP(false);
    }

    render() {
        let { status_button, status_input } = this.state;
        
        return (
            <>
            {
            
                status_button && <div className="button__log" onClick={this.activeCode}>Получить код</div>
            
            }
            {
                status_input && (
                    <div className="row__sendCode">
                        <div>
                            <input type="text" name="" id="code" className="input_send-activeCode" />
                            <div className="button_send-activeCode" onClick={this.newCode}>Отправить</div>
                        </div>
                    </div>
                )  
            }
                {/* <div className="button__log" onClick={this.activeCode}>Получить код</div> */}
            </>
        )
    }
}

export default ActiveIP;
