import React from 'react'

export const Paginations = ({banners, active, handler}) => {

    return (
        <div className="pagination" >
            {
                banners.map((page, index) =>(
                    <div key={index} className={(active !== index)? "number-page" : "number-page acive-page"} onClick={handler(index)}>{index+1}</div>)
                )
            }
        </div>
    )
}

export default Paginations;
