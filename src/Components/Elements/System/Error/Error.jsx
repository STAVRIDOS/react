import React from 'react';

import smile from './img/smile-search-white.png';
import './error.css';

export const Error = ({title}) => {
    return (
        <div className="error__search">
            <div className="error__info">
                <div className="title__error">
                    {title}
                </div>
                <div className="img__error">
                    <img src={smile} />
                </div>
            </div>
        </div>
    )
}

export default Error;
