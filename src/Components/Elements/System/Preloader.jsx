import React, { Component } from 'react';
import preloader from '../../../img/loading.gif';


export class Preloader extends Component {

    render() {
        let { children, loaded} = this.props;

        return (
            <>
            {
                loaded ? (
                    children
                ) : (
                    <div className="Preloader">
                        <img src={preloader} alt=""/>
                    </div>
                )
            }
            </>
        )
    }
}

export default Preloader;