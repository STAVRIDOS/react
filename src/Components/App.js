import React, { Component } from 'react';
import { Redirect } from 'react-router';
import { BrowserRouter } from "react-router-dom";
import { connect } from 'react-redux';
import { Helmet } from "react-helmet"

import Autorization from './Pages/Auth/Autorization';
import Home from './Pages/Home/Home';
import { Preloader } from './Elements';
import { authIt } from '../Actions';


export class App extends Component {


    componentDidMount(){
        this.props.authIt()
    }
 
    render() {
        let { user } = this.props;

        return (
            <>
                <Helmet>
                    <meta charSet="utf-8" />
                    <title>TUI-WEB</title>
                    <link rel="shortcut icon" href="https://www.tui.ua/images-new/icon/favicon-194x194.png" />
                </Helmet>
                <BrowserRouter >
                    <Preloader loaded={ user !== undefined ? true : false }>
                        <>
                            { (user !== undefined ? user.auth : false) ? <Home /> : <Autorization /> }
                            <Redirect to="/" />
                            <input type="text" id='copy'/>
                        </>
                    </Preloader>
                </BrowserRouter>
            </>
        )
    }
}

const MapStateToProps = state =>{
    return ({
        ...state.auth
    })
}

const mapDispatchToProps = (dispatch, getState) =>{
    return ({
        authIt: () => {
            dispatch(authIt())
        }
    })
}

export default connect(MapStateToProps, mapDispatchToProps)(App);

