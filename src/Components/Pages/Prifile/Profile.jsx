import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FaCamera } from 'react-icons/lib/fa';

import { updatePassword } from '../../../Actions'

import './profile.css';

export class Profile extends Component {

    state = {
        coincidences: true 
    }

    passOne = React.createRef();
    passTwo = React.createRef();

    savePass = () => {

        let status = this.passOne.current.value == this.passTwo.current.value ? true : false;

        this.setState({
            coincidences: status
        })
 
        if(status){
           this.props.updatePassword(this.passOne.current.value, this.props.user.id)
        }
    }
    UNSAFE_componentWillReceiveProps( nextProps){
        let { passwordStatus } = nextProps;
        
        console.log('passwordStatus', passwordStatus)
    }


    render() {
        let { user, passwordStatus } = this.props;
        let {passOne, passTwo, savePass } = this;
        
        console.log('passwordStatus', passwordStatus)
        
        return (
            <div className="row__profile">
                <h1>Профиль пользователя</h1>
                <div className="description__profile">
                    <div className="profile__photo">
                        <img src={user.photo} alt=""/>
                        <div className="update__photo center">
                            <FaCamera />
                        </div>
                    </div>
                    <div className="prfotile__info">
                        <div className="user__informations">
                            <span className="name__title center">Имя: </span>
                            <span className="center user_value">{user.name}</span>

                            <span className="surname__title center">Фамилия: </span>
                            <span className="center  user_value">{user.surname}</span>
                        
                            <span className="department__title center">Отдел: </span>
                            <span className="center  user_value">{user.department}</span>
                        
                            <span className="role__title center">Доступ: </span>
                            <span className="center  user_value">{user.role}</span>
                        </div>
                        {
                            !passwordStatus ?
                            <div className="user__data">
                                <div className="column__title center">Изменить пароль: </div>
                                <div className="data__line">
                                    <div className="data__title">Новый пароль:</div>
                                    <input type="password" ref={ passOne } />
                                </div>
                                <div className="data__line">
                                    <div className="data__title">Подтвердить пароль:</div>
                                    <input type="password" ref={ passTwo } />
                                </div>
                                {
                                    !this.state.coincidences && <div className="error__password">Пароли не совпадают</div>
                                }
                                {
                                    passwordStatus && <div className="update__password">OK</div>
                                }
                                <div className='save__button center' onClick={ savePass }>Подтвердить</div>
                            </div> :
                           
                                <div className="update__password">&#10003; Ваш пароль успешно изменён! </div>
                         
                        }
                        
                    </div>
                </div>

            </div>
        )
    }
}

const MapStateToProps = state =>{
    console.log('state', state)
    return ({ passwordStatus: state.auth.passwordStatus !== undefined ? state.auth.passwordStatus : false })
}


const mapDispatchToProps = (dispatch, getState) =>{
    return ({
        updatePassword: (password, id) => {
            dispatch(updatePassword(password, id))
        }
    })
}


export default connect(MapStateToProps, mapDispatchToProps)(Profile);
