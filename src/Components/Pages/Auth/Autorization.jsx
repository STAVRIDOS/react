import React, { Component } from 'react';
import { connect } from 'react-redux';

import { LogIn, temporaryIP } from '../../../Actions';
import { Logo, ActiveIP } from '../../Elements';

import './Autorization.css';

export class Autorization extends Component {

    sendAuth = ()=>{
        let { LogIn } = this.props;
        
        LogIn({
            method: 'post',
            auth: {
                username: document.querySelector('#login').value,
                password: document.querySelector('#password').value
            },
            withCredentials: true
        })
        this.setState({
            error: true
        })
    }

    activeIpCode = ()=>{

    }

    render() {
        let { user, temporaryIP } = this.props;

        return (
            <div className="row__autoriztion">
                <div className="auth__left">
                    <Logo />
                </div>
                <div className="outh__right">
                    <div className="outh__form">
                        <div className="auth__title">Пожалуйста, авторизируйтесь</div>
                            {
                                (user.ip !== undefined && !user.ip) ?  <div className="error_autoriztion">По вашему ІР адресу доступ запрещен</div>  :
                                !user.login ?  <div className="error_autoriztion">Неправильный логин</div> :
                                !user.password ?  <div className="error_autoriztion">Неправильный пароль</div> : ''
                            }
                            {
                                (user.ip !== undefined && user.ip) ?
                                (
                                    <>
                                        <div className="auth__inputs">
                                        <form>
                                            <p className="title__input">логин:</p>
                                            <input type="text" name="" id="login"/>

                                            <p className="title__input">пароль:</p>
                                            <input type="password" name="" id="password"/>
                                        </form>
                                        
                                    </div>
                                    <div className="button__log" onClick={this.sendAuth}>ВХОД</div>
                                    </>
                                ) : (
                                    <ActiveIP temporaryIP={temporaryIP} />
                                )
                            }
                    </div>
                </div>
                <div className="outh__circle"></div>
            </div>
        )
    }
}

Autorization.defaultProps = {
    user: {
        login: true,
        ip: true,
        password: true
    }
}

const MapStateToProps = state =>{
    return ({
        user: state.auth.user
    })
}

const mapDispatchToProps = (dispatch, getState) =>{
    return ({
        LogIn: user => {
            dispatch(LogIn(user))
        },
        temporaryIP: code =>{
            dispatch(temporaryIP(code))
        }
        
    })
}

export default connect(MapStateToProps, mapDispatchToProps)(Autorization);
// export default Autorization;
