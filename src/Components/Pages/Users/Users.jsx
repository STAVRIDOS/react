import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FaPencilSquare, FaPowerOff, FaUserPlus, FaTrashO } from 'react-icons/lib/fa';

import './users.css'

export class Users extends Component {
    render() {
        
        return (
            <div>
                <div className="header__users">
                    <div className="titile__page">Пользователи</div>
                    <div className="add__users">
                        <FaUserPlus />
                    </div>
                </div>
                <div className="user__line titles">
                    <div>№</div>
                    <div>Фото</div>
                    <div>Пользователь</div>
                    <div>mail</div>
                    <div>Права</div>
                    <div>Насторойки</div>
                </div>
                {
                    this.props.users.map( (user, index) =>(
                        <div className="user__line">
                            <div className="user__count">{index+1}</div>
                            <div className="user__img">
                                <img src={user.img} alt=""/>
                            </div>
                            <div className="user__name">{user.name} {user.surname}</div>
                            <div className="user__department">{user.mail}</div>
                            <div className="user__status">{user.role}</div>
                            <div className="settings__user">
                                <div className="edit__user">
                                    <FaPencilSquare />
                                </div>
                                <div className={user.status ? "disconnect__user user__active" : "disconnect__user"}>
                                    <FaPowerOff />
                                </div>
                                <div className="delite_user">
                                    <FaTrashO />
                                </div>
                            </div>
                        </div>
                    ))
                }
            </div>
        )
    }
}

const MapStateToProps = state =>{
    console.log(state)
    return ({
        users: state.users
    })
}

const mapDispatchToProps = (dispatch, getState) =>{
    return ({})
}
Users.defaultProps = {
    users: [
        {
            name: 'Name',
            surname: 'Surname',
            mail: 'test@tui.ua',
            role: 'ADMIN',
            img: 'https://i7.pngguru.com/preview/416/62/448/login-google-account-computer-icons-user-activity.jpg',
            status: true
        },
        {
            name: 'Name',
            surname: 'Surname',
            mail: 'test@tui.ua',
            role: 'ADMIN',
            img: 'https://i7.pngguru.com/preview/416/62/448/login-google-account-computer-icons-user-activity.jpg',
            status: false
        },
        {
            name: 'Name',
            surname: 'Surname',
            mail: 'test@tui.ua',
            role: 'ADMIN',
            img: 'https://i7.pngguru.com/preview/416/62/448/login-google-account-computer-icons-user-activity.jpg',
            status: true
        }
    ]
}

export default connect(MapStateToProps, mapDispatchToProps)(Users);

