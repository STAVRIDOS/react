import React, { Component } from 'react';

import './button.css';

export class Buttons extends Component {
    state = {
        activeIndex: this.props.activeButton,
        regions: []
    }

    buttonClick = (button, index) => ()=>{
     
        this.setState({
            activeIndex: index,
            activeSite: button
        })
        this.props.handler(index)
    }

    UNSAFE_componentWillReceiveProps( nextProps){
        if(this.props.activeIndex !== nextProps.activeButton){
            this.setState({
                activeIndex: nextProps.activeButton,
            })
        }

    }

    render() {
        let { activeIndex } = this.state
        let { buttons } = this.props;

        return (
            <div className="row_buttons-regions" >
                {
                    buttons.map((button, key)=>{     
                        
                        return (
                            <div key={key} className={activeIndex == key ? "button__region active_region" : "button__region "}  onClick={this.buttonClick(button, key)}>
                                {button}
                            </div>
                        )
                    })
                }
            </div>
        )
    }
}

export default Buttons
