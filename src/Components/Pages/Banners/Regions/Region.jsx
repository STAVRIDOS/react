import React, { Component } from 'react';
import { Buttons, DropList } from './';

import './regions.css'


export class Region extends Component {
    state = {
        regions: [
            {name: 'Санкт-Петербург', code: 'spb', checked: false, id: 0}, 
            {name: 'Москва', code: 'mow' ,checked: false, id: 1}, 
            {name: 'Самара', code: 'Smr',checked: false, id: 2}, 
            {name: 'Уфа', code: 'Ufa',checked: false, id: 3}, 
            {name: 'Екатеринбург', code: 'Ekb',checked: false, id: 4}, 
            {name: 'Челябинск', code: 'Chel',checked: false, id: 5}, 
            {name: 'Новосибирск', code: 'Nvs',checked: false, id: 6}, 
            {name: 'Ростов-на-Дону', code: 'RstnDon',checked: false, id: 7}, 
            {name: 'Минеральные Воды', code: 'MnrlVd',checked: false, id: 8}, 
            {name: 'Краснодар', code: 'kdar',checked: false, id: 9}, 
            {name: 'Белгород', code: 'Belgorod',checked: false, id: 10}, 
            {name: 'Нижневартовск', code: 'Nizhnevartovsk',checked: false, id: 11}, 
            {name: 'Красноярск', code: 'Krasnoyarsk',checked: false, id: 12}, 
            {name: 'Пермь', code: 'Prm',checked: false, id: 14},
            {name: 'Калининград', code: 'Kln',checked: false, id: 15},
            {name: 'Казань', code: 'Kzn',checked: false, id: 16},
            {name: 'Нижнекамск', code: 'Nizhn',checked: false, id: 17},
            {name: 'Тюмень', code: 'Tum',checked: false, id: 18},
            {name: 'Кемерово', code: 'Kemerovo',checked: false, id: 19},
            {name: 'Волгоград', code: 'Volgograd',checked: false, id: 20},
            {name: 'Сургут', code: 'Surgut',checked: false, id: 21},
            {name: 'Воронеж', code: 'Vrn',checked: false, id: 22},
            {name: 'Барнаул', code: 'Barnaul',checked: false, id: 23},
            {name: 'Ульяновск', code: 'Ulsk',checked: false, id: 24},
            {name: 'Архангельск', code: 'Arkhangelsk',checked: false, id: 25},
            {name: 'Сыктывкар', code: 'Stk',checked: false, id: 26},
            {name: 'Томск', code: 'Tomsk',checked: false, id: 27},
            {name: 'Магнитогорск', code: 'Magnitogorsk',checked: false, id: 28},
            {name: 'Омск', code: 'Omsk',checked: false, id: 29},
            {name: 'Оренбург', code: 'Oren',checked: false, id: 30},
            {name: 'Хабаровск', code: 'Habarovsk',checked: false, id: 31},
            {name: 'Иркутск', code: 'Irkutsk',checked: false, id: 32},
            {name: 'Владикавказ', code: 'Vladikavkaz',checked: false, id: 33},
            {name: 'Мурманск', code: 'Mrm',checked: false, id: 34},
            {name: 'Саратов', code: 'Sar',checked: false, id: 35},
            {name: 'Нижний Новгород', code: 'NN',checked: false, id: 36}
        ],
        activeButton: this.props.active
    }

    componentWillMount(){
        let resultSort = [];

        if(this.state.activeButton == 0){
            resultSort = this.state.regions.map(el => ({...el, checked: true}))
        }else if(this.state.activeButton == 1){
            resultSort = this.state.regions
        }

        if(this.props.site !== 'agent.tui.ru'){
                resultSort = [{name: 'Все регионы', code: '', checked: true, id: 0}] 
        }

        this.setState({
            regions: resultSort
        })
    }
    


    checkedRegion = e => {
        let regions = this.state.regions;
        let regionsDb = [];

        regions.forEach(el => {
            if(el.id == e.target.dataset.id){
                el.checked = !el.checked
            }

            if(el.checked){
                regionsDb.push(el.code)
            }
        })

        this.setState({
            regions: regions,
            activeButton: 1,
        })

        this.props.addRegions(regionsDb.join(';'))
        
    }

    avtiveButton = index => {
        let regions = [];
        let regionsDb = [];
        

        if(index !== this.state.activeButton && index == 1){
            regions = this.state.regions.map(element =>{  
                        element.checked = false;
                        return element;
                    });
        }else if(index == 0){
            regions = this.state.regions.map(element =>{  
                element.checked = true;
                regionsDb.push(element.code);
                
                return element;
            });
        }

        this.props.addRegions(regionsDb.join(';'))
        this.setState({
            activeButton: index,
            regions: regions
        })
    }; 

    UNSAFE_componentWillReceiveProps( nextProps){
        if(this.props.regions !== nextProps.regions){
            
            this.setState({
                regions: this.state.regions.map( element => {
                    let reg = new RegExp(element.code, 'g');

                    if(reg.test(nextProps.regions)){
                        element.checked = true
                    }else{
                        element.checked = false
                    }
                    return element;
                })
            })
        }


        if(this.props.active !== nextProps.active){

            this.setState({
                activeButton: nextProps.active,
            })

        }

    }


    render() {
        let { regions, activeButton } = this.state;

        return (
            <>
                <Buttons buttons={['Все', 'Несколько']} activeButton={activeButton} handler={this.avtiveButton}/>
                <div className="row__dropList active__dropList">
                    <div className="drop__title">Выбрать регион:</div>
                    <DropList checkedRegion={this.checkedRegion} regions={regions}/>
                </div>
           
            </>
        )
    }
}

export default Region;
