import React, { Component } from 'react';

import './dropList.css'

export class DropList extends Component {
    state = {
        allRegions: this.props.regions,
        resultRegions: this.props.regions
    }

    search = e =>{
        let { allRegions, resultRegions } = this.state;

        let reg = new RegExp(e.target.value.toLowerCase(),'g');
        let resultSearch = allRegions.filter(element => reg.test(element.name.toLowerCase()));

        this.setState({
            resultRegions: resultRegions.length == 0 ? allRegions : resultSearch,
        })
    }

    render() {

        let { resultRegions } = this.state;
        return (
            <>
                <input type="text" className="search__regions" placeholder="Введите название региона" onChange={this.search}/>
                <div className="row__regions-drop">
                    {
                        resultRegions.map( region =>(
                            <div key={region.id}  className={region.checked ? 'line__region' : 'line__region-disable'}>
                                <input type="checkbox" data-id={region.id} checked={region.checked} onClick={this.props.checkedRegion}/>
                                <span>{region.name}</span>
                            </div>
                        ))
                    }
                </div>
            </>
        )
    }
}

export default DropList;

