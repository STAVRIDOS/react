import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom'
import {  FaEyeSlash, FaArrowRight, FaArrowLeft,FaTimesCircleO } from 'react-icons/lib/fa';

import { authIt, sortBanners, getBanners, activePage, allDelite, allVisibility, sortTypeBanners } from '../../../Actions'
import {Button ,  Preloader, Paginations, Error} from '../../Elements';

import BannersList from './BannersList';

import './banners.css';


export class Banners extends Component {
    state = {
        loaded: true,
        allBanners: [],
        searchBanners: [],
        buttonFooter: false
    }

    setCheckBanners = () => e =>{
 
        
        let { allBanners } = this.state;
        let id = Number(e.target.dataset.id);
        let type = e.target.dataset.type;
        let site = e.target.dataset.site;
        let serial =  Number(e.target.dataset.serial);
        let array = []

        if(e.target.checked){

            array = [
                ...allBanners,
                {
                    id: id,
                    type: type,
                    site: site,
                    serial: serial
                }

            ]
        }else{

            array = allBanners.filter(el => el.id !== id)
            
        }
 
        this.setState({
            ...this.state,
            allBanners: [
                ...array
            ],
            buttonFooter: array.length > 0 ? true : false
        })
    }
    delete = () =>{
        this.props.allDelite(this.state.allBanners);
        this.setState({
            allBanners: []
        })
    
    }

    hide = () =>{
        this.props.allVisibility(this.state.allBanners);
        this.setState({
            allBanners: []
        })
    }

    componentDidMount(){
        let {getBanners, activeSite} = this.props;
    }

    UNSAFE_componentWillReceiveProps( nextProps){
        let {activeSite, aciveTypeBanner, sortTypeBanners, allBanners} = this.props;
 

        if(allBanners !== nextProps.allBanners){
            sortTypeBanners(aciveTypeBanner)
        }

        if(activeSite !== nextProps.activeSite){
            this.setState({
                allBanners: []
            })
        }
    }


    render() {
        let { resultBanners, activePage, match, page, loaded, sortTypeBanners, aciveTypeBanner, activeSite, getBanners, allBanners, user  } = this.props;
        let buttons = user.role == 'administrator' ? 
            ['agent.tui.ru', 'agent.tui.ua', 'agent.tui.by','b2b.tui.ru','b2b.tui.ua', 'b2b.tui.by'] : ['agent.tui.ua','b2b.tui.ua'];

        return (
            <>
                <div className="header__banners">
                    <Button 
                        handler={getBanners}  
                        buttons={buttons} 
                        active={this.props.activeSite}
                        loaded={loaded}
                        title="Выбрать сайт"
                    />
                    <Button 
                        handler={sortTypeBanners}  
                        buttons={['слайдер', 'поп-ап']} 
                        active={this.props.aciveTypeBanner}
                        loaded={loaded}
                        title="Выбрать тип"
                    />
                    <div>
                    
                </div>
                <div className="add__banner">
                    {aciveTypeBanner === false || activeSite === false ? <span></span> :
                        <NavLink to='banners/new' className="button__blue"> Добавить </NavLink>
                    }        
                </div>
                
                    
                </div>
                    
                <Preloader loaded={ loaded } type="page">
                    {
                        resultBanners.length > 0 && aciveTypeBanner ?
                        <>
                        <div className="banners_list-title">
                            <div className="banner__check"> </div>
                            <div className="banner__changes"> </div>
                            <div className="banner__name crs"> </div>
                            <div className="banner__type"> Баннер </div>
                            <div className="banner__type"> Тип баннера </div>
                            <div className="banner__sit"> Порядковый номер </div>
                            <div className="banner__visibility "> Видимость </div>
                            <div className="banner__start-date"> Дата начала показа </div>
                            <div className="banner__finish-date"> Дата завершения показа </div>
                        </div>
                        <BannersList 
                            banners={resultBanners[activePage]} 
                            match={ match } 
                            updateBanners={this.props.getBanners}
                            handler = {this.setCheckBanners}
                            checkedBanners = {this.state.allBanners}
                        /> 
                        <div className="banners__footer">
                            <div className="changes__all">
                                <div className={this.state.buttonFooter ? "banners__del" : "banners__del disable__button"} 
                                    onClick={this.state.buttonFooter ? this.delete : ()=>{console.log('')}}>
                                <FaTimesCircleO /> удалить
                                </div>
                                <div className={this.state.buttonFooter ? 'banners__visibility' : 'banners__visibility disable__button'} 
                                    onClick={this.state.buttonFooter ? this.hide : ()=>{console.log('')}}>
                                    <FaEyeSlash /> скрыть
                                </div>
                            </div>
                            <Paginations banners={resultBanners} active={activePage} handler={page}/>
                            <div className="buttons__navigation">
                                <div className="na__left" onClick={page(activePage-1)}>
                                    <FaArrowLeft />
                                </div>
                                <div className="na__right" onClick={page(activePage+1)}>
                                    <FaArrowRight />
                                </div>
                            </div>
                        </div>
                        </>
                        : resultBanners.length === 0 && aciveTypeBanner ?
                           <Error title='баннера не найдены' />
                        
                        : allBanners.length > 0 && !aciveTypeBanner ?
                            <Error title='НЕ ВЫБРАН ТИП БАННЕРА' />
                        : 
                        <Error title='НЕ ВЫБРАН САЙТ' />
                    }
                </Preloader>
            </>
        )
    }
}


const MapStateToProps = state =>{
    // console.log(state.auth.user)
    return ({
        user: state.auth.user,
        ...state.banners,
        resultBanners: state.banners.resultBanners,
        activeSite: state.banners.activeSite,
        loaded: state.banners.loaded
    })
}


const mapDispatchToProps = (dispatch, getState) =>{
    return ({
        authIt: () => {
            dispatch(authIt())
        },
        getBanners: value => {
            dispatch(getBanners(value))
        },

        sortBanners: value => {
            dispatch(sortBanners(value))
        },
        page: number => () => {
            dispatch(activePage(number))
        },
        allDelite: arr => {
            dispatch(allDelite(arr))
        },
        allVisibility: arr => {
            dispatch(allVisibility(arr))
        },
        sortTypeBanners: type => {
            dispatch(sortTypeBanners(type))
        },
    })
}


export default connect(MapStateToProps, mapDispatchToProps)(Banners);
