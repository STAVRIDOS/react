import React from 'react';
import { NavLink } from 'react-router-dom';

import { FaPencil, FaEye, FaEyeSlash, FaClockO  } from 'react-icons/lib/fa';

export const BannersList = ({banners, match, handler, checkedBanners}) => {

    
    


    return (
        <div className="back_white">

            {
            banners.map((banner, index)=>{
                    let option = {
                        month: 'long',
                        day: 'numeric',
                        timezone: 'UTC',
                        hour: 'numeric',
                        minute: 'numeric',
                    }
                 
                    let checkedList = id => {
                     
                        let status = false;



                        checkedBanners.map(el=>{

                            if(!status){
                                status = el.id == id ? true : false
                            }
                            
                        })
                
                        return status;
                    }
                    
                    let checkId = checkedList(banner.id);
                    let date = new Date().getTime();
                    let start = Number(banner.date_start);
                    let finish = Number(banner.date_finish)
                    
                    return (
                        <div key={index} className={checkId ? "banners_list check_row" : 'banners_list'}>
                            <div className="banner__check">
                                <input 
                                    className="crs" 
                                    type="checkbox" 
                                    
                                    data-id = { banner.id } 
                                    data-type = { banner.type } 
                                    data-site = { banner.site } 
                                    data-serial = { banner.serial_number } 
                                    onChange = { handler() }
                                    checked = {checkId}
                                />
                            </div>
        
                            <NavLink to={`${match.path}/${index}`} className="banner__changes" >
                             
                                < FaPencil />
                            </NavLink>

                            <NavLink to={`${match.path}/${index}`} className="banner__name crs" >
                                <span className="banner__name-text">{banner.name}</span>
                            </NavLink>
                            <div className="banner__preview">
                                <img style={{height: '19px'}} src={banner.img} alt=""/>
                            </div>
                    
                            <div className="banner__type">
                                {banner.type}
                            </div>
                            <div className="banner__sit">
                                {banner.serial_number}
                            </div>
                            <div className={banner.visibility && finish > date ? "banner__visibility active" : 'banner__visibility disable'} data-id={banner.id} onClick={handler()}>
              
                                {
                                     start > date && banner.visibility ? 
                                        <FaClockO /> 
                                    : start < date && banner.visibility && finish > date ?
                                        <FaEye /> :
                                    finish < date && banner.visibility ? <FaEyeSlash /> :
                                        <FaEyeSlash /> 
                                }
                            </div>
                            <div className="banner__start-date">
                                {new Date(Number(banner.date_start)).toLocaleString('ru', option)}
                            </div>
                            <div className="banner__finish-date">
                                {new Date(Number(banner.date_finish)).toLocaleString('ru', option)}
                            </div>
                        </div>
                    )
                })
            }
            
        </div>
    )
}

BannersList.defaultProps = {
    banners: []
}

export default BannersList
