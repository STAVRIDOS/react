import React, { Component } from 'react';
import { connect } from 'react-redux';
import DatePicker from "react-datepicker";
import { FaEye, FaEyeSlash  } from 'react-icons/lib/fa';
import { DeleteButton, Button } from '../../Elements';
import Region from './Regions/Region'
import { addBanners, updateBanners, deleteBanners } from '../../../Actions';




import '../../../style/editBanners.css';
import "react-datepicker/dist/react-datepicker.css"
import 'react-datepicker/dist/react-datepicker-cssmodules.min.css';

export class EditBanners extends Component {

    state = {
        date: new Date(),
        regions: []
      };
     
      startDate = date => {
        this.setState({
            start: new Date(date)
        });
      };

      finishDate = date => {
        this.setState({
            finish: new Date(date)
        });
        
      };

      start = () => {
        this.setState({
            start: new Date()
        });
        
      };

      finish = () => {
        this.setState({
            finish: new Date()
        });
        
      };

      ga = React.createRef();

      toDataURL = () => {

        var filesSelected = document.getElementById("inputFileToLoad").files;
        console.log(filesSelected)
        if (filesSelected.length > 0){
            var fileToLoad = filesSelected[0];
    
            var fileReader = new FileReader();
            let set = url => {
                this.setState({
                    img: url
                })
            }
            fileReader.onload = function(fileLoadedEvent) {
                var srcData = fileLoadedEvent.target.result;
                set(srcData)
            }
    
            fileReader.readAsDataURL(fileToLoad);

        }
    }
    

    newName = () => e =>{
        this.setState({
            name: e.target.value
        })
    }

    newLink= () => e =>{
        this.setState({
            link: e.target.value
        })
    }

    newSerial= () => e =>{
        this.setState({
            serial: Number(e.target.value)
        })
    }
    

    updateSite = value =>{
        this.setState({
            site: value
        })
    }
    updateType = value =>{
        this.setState({
            type: value
        })
    }


    visibilityUpadate = status => () =>{
        this.setState({
            visibility: status
        })
    }

    copyLink = () =>{
        let inp = document.querySelector('#copy');
            inp.value = this.state.img;
            inp.select()
            document.execCommand('copy');
  
    }

    saveBanner = () =>{
        this.setState({
            ga: this.ga.current.value
        })
    }

    addRegions = regions => {
        this.setState({
            regions: regions
        })
    }

    componentDidMount(){

        const { resultBanners, match, active, activeSite, activeType } = this.props;

            if(resultBanners.length > 0 && resultBanners[active][Number(match.params.id)] !== undefined){
                let banner = resultBanners[active][Number(match.params.id)];
                this.setState({
                    name: banner.name,
                    link: banner.link,
                    img: banner.img,
                    start: new Date(Number(banner.date_start)),
                    finish: new Date(Number(banner.date_finish)),
                    type: banner.type,
                    site: banner.site,
                    id: banner.id,
                    serial: banner.serial_number,
                    regions: banner.regions !== '' ? banner.regions : '',
                    index: Number(match.params.id),
                    visibility: banner.visibility,
                    ga: banner.ga
        
                })
            }else {
                let date = new Date();
                let serial = resultBanners.length > 0 ? Number(resultBanners[active][0].serial_number)+1 : 1;
     
                this.setState({
                    name: '',
                    link: '',
                    img: 'http://aginskoe24.ru/upload/004/u439/79/70/kommunikativnaja-kompetentnost-o-nabolevshem-photo-big.jpg',
                    start: date,
                    finish: new Date(date.getFullYear()+2, 0, 0, 0, 0, 0),
                    type: activeType,
                    site: activeSite,
                    id: 0,
                    serial: serial,
                    regions: '',
                    index: false,
                    visibility: true,
                    ga: ''
        
                })
            }
    }  

    render() {
        let { copyLink } = this;
        let { addBanners, match, updateBanners, deleteBanners, activeSite } = this.props;
        let handler = (match.url !== '/banners/new') ? updateBanners : addBanners;
        console.log('regions-edit', this.state.regions)
        return (
            <div className="idit__banners-row">
                <div className="edit__description">
                    <div className="edit-description-left">
                        <p>Название:</p>
                        <input 
                            className="edit__name" 
                            type="text" 
                            value={this.state.name}
                            placeholder="Название баннера"
                            onChange={this.newName()}
                        />
                        <div className="rowTypesBanners">
                            <div  className='editBannersSites'>
                                <Button 
                                    handler={this.updateSite} 
                                    buttons={['agent.tui.ru', 'agent.tui.ua', 'agent.tui.by','b2b.tui.ru','b2b.tui.ua', 'b2b.tui.by']}
                                    active={this.state.site}
                                />
                            </div>
                            <div style={{width: '450px'}}>
                                <Button 
                                    handler={this.updateType} 
                                    buttons={['слайдер', 'поп-ап']}
                                    active={this.state.type}
                                />
                            </div>
                        </div>
                        <div className="row_inputs">
                            <div className="link__banners">
                                <p>Ссылка с баннера:</p>
                                <input 
                                    className="edit__name" 
                                    type="text" 
                                    value={this.state.link} 
                                    placeholder="Ссылка с баннера"
                                    onChange={this.newLink()}/>
                            </div>
                            <div className="analytics">
                                <p>Google analytics:</p>
                                <input 
                                    id="ga" 
                                    className="edit__name" 
                                    ref={this.ga} 
                                    type="text" 
                                    value={this.state.ga}
                                    placeholder="Только код кнопки: click_button_N2" 
                                    onChange={this.saveBanner}/>
                            </div>
                        </div>
                    </div>
                    <div className="edit-description-right bRight">
                        <p>Показывать от:</p>
                        <div className="date-block">
                            <DatePicker
                                className='date-input'
                                selected={ this.state.start }
                                onChange={ this.startDate }
                                showTimeSelect
                                timeFormat="H:mm"
                                timeIntervals={ 30 }
                                dateFormat="dd/MM/yyyy H:mm"
                            />
                            <button className="date-button" onClick = { this.start } >Сейчас</button>
                        </div>
                        <div className="date-block">
                            <p>Показывать до:</p>
                            <DatePicker
                                className='date-input'
                                selected={this.state.finish}
                                onChange={this.finishDate}
                                showTimeSelect
                                timeFormat="H:mm"
                                timeIntervals={30}
                                dateFormat="dd/MM/yyyy H:mm"
                            />
                             <button className="date-button" onClick = { this.finish } >Сейчас</button>
                        </div>
                        <div className="visibility__row">
                            <div className="visibility__text">
                                Видимость баннера:
                            </div>
                            <div className="visibility_buttons">
                                <div data-vis={true} 
                                    className={this.state.visibility ? "vis__true vis visibility_true" : "vis vis__true"} 
                                    onClick={this.visibilityUpadate(true)} 
                                >
                                < FaEye />
                                </div>
                                <div data-vis={false} className={this.state.visibility ? "vis__false vis" : "vis__false visibility_true vis"} 
                                onClick={this.visibilityUpadate(false)}>
                                    <FaEyeSlash />
                                </div>
                            </div>
                        </div>
                        <div className="serial-number">
                            <div className="serial-number__text">
                                № баннера по порядку:
                            </div>
                            <div className="serial-number__input">
                                <input type="number" name="" id="" value={this.state.serial} onChange={this.newSerial()}/>
                            </div>
                        </div>
                    </div>
                    <div className="edit-description-right">
                        <Region 
                            active={this.state.regions.length === 0 ? 0 : 1} 
                            regions={this.state.regions} 
                            site={activeSite} 
                            addRegions={this.addRegions}
                            regions={this.state.regions}
                        />
                    </div>
                </div>
                <div className="edit__img">
                    <input id="inputFileToLoad" name="img" type="file" onChange={this.toDataURL} />
                    <label className='button_download_img' htmlFor="inputFileToLoad">Загрузить картинку на сайт</label>                
                </div>
                <div className="vis__img">
                    <img src={this.state.img} alt=""/>
                </div>
                <div className="buttons__img">
                    <div className='button_download_img' onClick={copyLink}>Скопировать ссылку на картинку</div>
                    <a href={this.state.img} download={`${this.state.name}.jpg`} className='button_download_img'>Сохранить изображение</a>
                </div>
                <div className="buttons__result">
                    <DeleteButton handler={deleteBanners} banner={this.state} type='del' />
                    <DeleteButton handler={handler} banner={this.state} type='save'/>
                </div>
            </div>
        )
    }
}



EditBanners.defaultProps = {
    banners: {
        banners: []
    },
    resultBanners: [[]]
}

const MapStateToProps = state =>{

    return ({
        banners: [
            ...state.banners.allBanners,
            
        ],
        resultBanners: [
            ...state.banners.resultBanners
        ],
        active: state.banners.activePage,
        activeSite: state.banners.activeSite,
        activeType: state.banners.aciveTypeBanner
    })
}

const mapDispatchToProps = (dispatch, getState) =>{
    return ({
        addBanners: banner => () => {
            dispatch(addBanners(banner))
        },
        updateBanners: banner => ()=>{
            dispatch(updateBanners(banner))
        },
        deleteBanners: banner => () => {
            dispatch(deleteBanners(banner))
        }
    })
}


export default connect(MapStateToProps, mapDispatchToProps)(EditBanners);