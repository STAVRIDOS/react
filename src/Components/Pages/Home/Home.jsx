import React, { Component } from 'react';
import { Route, Switch } from "react-router-dom";
import { connect } from 'react-redux';
import { FaAngleDoubleRight, FaAngleDoubleLeft } from 'react-icons/lib/fa';
import { LogOut } from '../../../Actions';


import { Logo, LeftMenu } from '../../Elements';
import TopMenu from '../../Elements/Menu/Header/TopMenu';
import EditBanners from '../Banners/EditBanners';
import Banners from '../Banners/Banners';
import Users from '../Users/Users';
import Pages from '../Pages/Pages';
import Info from '../Info/Info'
import Profile from '../Prifile/Profile'
import { authIt, getData, getBanners } from '../../../Actions';


import './Home.css';

export class Home extends Component {
    state = {
        mobile: false
    }

    mobile_menu = () =>{
        this.setState({
            mobile: !this.state.mobile
        })
    }

    componentDidMount(){
        // this.props.users_online()
    }
    render() {
        let { user, news, getData, getBanners, usersOnline, users_online, activeSite} = this.props;
        let { mobile } = this.state;
     
        return (
            <div className="row__autoriztion">
                <div className="menu__mobile" onClick={this.mobile_menu}>
                    { mobile ? <FaAngleDoubleLeft /> : <FaAngleDoubleRight /> }
                </div>
                <div className={mobile ? "auth__left active__menu-mobile" : 'auth__left'}>
                    
                    <Logo />
                    <LeftMenu 
                        user={user} 
                        // send={getData}
                        getBanners={getBanners}
                        site = {activeSite}
                        mobile={mobile}
                        users = {usersOnline}
                    />
                </div>
      
                <div className="home_right">
                    <TopMenu user = { user }  />
                    <div className="home_body">
                        <div className="row__info">
                            <Switch>
                                <Route exact path="/" render={()=>(<Info news = {news} />)}/>
                                <Route exact path="/banners" render={props=>(<Banners {...props} usersOnline={users_online}/>)}/>
                                <Route exact path="/banners/:id" render={props=>(<EditBanners {...props} usersOnline={users_online}/> )}/>
                                <Route exact path="/banners/new" render={props=>(<EditBanners {...props} usersOnline={users_online}/>)}/>
                                <Route path="/forms" render={()=>(<Pages />)}/>
                                <Route path="/parser" render={()=>(<h1>Parser</h1>)}/>
                                <Route path="/service" render={()=>(<h1>Service</h1>)}/>
                                <Route path="/settings" render={()=>(<h1>Settings</h1>)}/>
                                <Route path="/pages" render={()=>(<h1>Pages</h1>)}/>
                                <Route path="/users" render={()=>(<Users />)}/>
                                <Route path="/profile" render={ props =>(<Profile user={user}/>)}/>
                            </Switch>
                        </div> 
                    </div>
                </div> 
            </div>
        )
    }
}

Home.defaultProps = {
    news: [
        {
            title: 'Редактирование товара',
            date: '31/10/19',
            use: 'Администратор, Редактор',
            text: ``
        }
    ]
}

const MapStateToProps = state =>{
    return ({
        ...state.auth,
        activeSite: state.banners.activeSite
    })
}

const mapDispatchToProps = (dispatch, getState) =>{
    return ({
        authIt: () => {
            dispatch(authIt())
        },
        getData: ()=>{
            dispatch(getData())
        },
        getBanners: site =>{
 
            dispatch(getBanners(site))
        },
     
        LogOut: () => {
            dispatch(LogOut())
        }
    })
}


export default connect(MapStateToProps, mapDispatchToProps)(Home);
