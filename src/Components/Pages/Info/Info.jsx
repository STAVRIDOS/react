import React from 'react';
import { Preloader } from '../../Elements'

import './info.css';

export const Info = ({news}) => {
    return (
        <Preloader loaded={true}>
            <h1>Последние обновления</h1>
            {
                news.map((n,k) => (
                    <div key={k}>
                        <div className="title_new">{n.title}</div>
                            <div className="news-date">
                                <span className="date_new">
                                    {n.date}
                                </span>
                                <span className="role_new">
                                    Доступно пользователям с правами: {n.use}
                                </span>
                                <span className="time_new"></span>
                            </div>
                        <div className="text_new">{n.text}</div>
                    </div>
                ))
            }
        </Preloader>
    )
}


export default Info;
