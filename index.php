<?php

// header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Origin, X-Auth-Token , Authorization');

$result_data = [];

$data = json_decode(file_get_contents('php://input'));

include_once('server/config.php');

echo json_encode($result_data);